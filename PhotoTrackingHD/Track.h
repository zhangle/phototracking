//
//  Track.h
//  GPSLogger
//
//  Created by NextBusinessSystem on 12/01/26.
//  Copyright (c) 2012 NextBusinessSystem Co., Ltd. All rights reserved.
//

@class TrackPoint;

@interface Track : NSObject {
    NSString * track_id;
    NSString * name;
    NSDate * timestamp;
    NSMutableSet *trackpoints;
}

@property (nonatomic, retain) NSString * track_id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSDate * timestamp;
@property (nonatomic) NSInteger distance;
@property (nonatomic) NSInteger elapsedTime;
@property (nonatomic, retain) NSMutableSet *trackpoints;

- (Track*) init;

- (NSArray *)sotredTrackPoints;

- (void)addTrackpointsObject:(TrackPoint *)value;
@end

