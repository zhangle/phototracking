//
//  FirstViewController.h
//  PhotoTrackingHD
//
//  Created by 张 乐 on 12-8-20.
//  Copyright (c) 2012年 张 乐. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "AppDelegate.h"
#import "QBKOverlayMenuView.h"

#define START_BUTTON_TEXT	@"Start"
#define STOP_BUTTON_TEXT	@"Stop"


@class AppDelegate;

@interface StatsViewController : UIViewController <QBKOverlayMenuViewDelegate> {
    QBKOverlayMenuView *_qbkOverlayMenu;
}

@property (retain, nonatomic) IBOutlet UIView *infoView;
@property (retain, nonatomic) IBOutlet MKMapView *mapView;
@property (retain, nonatomic) IBOutlet UILabel *runTimeLabel;
@property (retain, nonatomic) IBOutlet UILabel *distanceLabel;
@property (retain, nonatomic) IBOutlet UILabel *altitudeLabel;
@property (retain, nonatomic) IBOutlet UILabel *avgSpeedLabel;
@property (retain, nonatomic) IBOutlet UILabel *curSpeedLabel;
@property (retain, nonatomic) IBOutlet UIButton *startButton;
@property (retain, nonatomic) IBOutlet UIButton *resetButton;

@property (nonatomic, retain) AppDelegate* delegate;
@property BOOL started;

- (IBAction)startOrStop;
- (void)reset;

-(void)setStartButtonTitle:(NSString*)text;
-(void)updateRunDisplay;
-(void)annotateMap:(CLLocation*)location;
-(void)drawLineForLocations:(CLLocation*)location fromLocation:(CLLocation*)oldLocation;

-(void)addAnnotation:(MKPointAnnotation*)annotation;

@end
