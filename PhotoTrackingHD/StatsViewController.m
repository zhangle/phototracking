//
//  FirstViewController.m
//  PhotoTrackingHD
//
//  Created by 张 乐 on 12-8-20.
//  Copyright (c) 2012年 张 乐. All rights reserved.
//

#import "StatsViewController.h"
#import "StartPointAnnotation.h"
#import "EndPointAnnotation.h"

@interface StatsViewController (MKMapViewDelegate) <MKMapViewDelegate>

@end

@implementation StatsViewController

@synthesize delegate;
@synthesize started;
@synthesize infoView;
@synthesize mapView;
@synthesize runTimeLabel;
@synthesize distanceLabel;
@synthesize altitudeLabel;
@synthesize avgSpeedLabel;
@synthesize curSpeedLabel;
@synthesize startButton;
@synthesize resetButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	
	// Initialize settings
	NSLog(@"Initializing settings");
	if ([self.delegate loadSettings] == NO) {
		self.delegate.isMetric = YES;
		self.delegate.sensitivity = DEFAULT_SENSITIVITY;
        self.delegate.isBackground = NO;
        self.delegate.isRunning = NO;
        
		[self.delegate saveSettings];
        
	}
	
    QBKOverlayMenuViewOffset offset;
    offset.topOffset = 0;
    
    _qbkOverlayMenu = [[QBKOverlayMenuView alloc] initWithDelegate:self position:kQBKOverlayMenuViewPositionTop offset:offset];
    [_qbkOverlayMenu setParentView:[self view]];
    
    [_qbkOverlayMenu addSegmentButton];
    [_qbkOverlayMenu addMyLocationButton];
    
	[self reset];
    
    [self.delegate loadMapType:self.mapView];
    [_qbkOverlayMenu setSelectedButton:self.mapView.mapType];
    
    self.infoView.backgroundColor = [UIColor colorWithHue:0.0 saturation:0.0 brightness:1.0 alpha:0.4];
}

-(void)myCustomSelector
{
    
}

- (void)viewDidUnload
{
    [self setMapView:nil];
    [self setRunTimeLabel:nil];
    [self setDistanceLabel:nil];
    [self setAltitudeLabel:nil];
    [self setAvgSpeedLabel:nil];
    [self setCurSpeedLabel:nil];
    [self setStartButton:nil];
    [self setResetButton:nil];
    [self setInfoView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
    [_qbkOverlayMenu release];
    _qbkOverlayMenu = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)dealloc {
    [mapView release];
    [runTimeLabel release];
    [distanceLabel release];
    [altitudeLabel release];
    [avgSpeedLabel release];
    [curSpeedLabel release];
    [startButton release];
    [resetButton release];
    [infoView release];
    [super dealloc];
}

- (IBAction)startOrStop {
    if (self.started == NO) {
        if ([self.delegate checkLocationServiceSupported]) {
            // Start new run
            NSLog(@"Starting or resuming run");
            self.started = YES;
            [self setStartButtonTitle:STOP_BUTTON_TEXT];
            [self.delegate start];
        }
        else {
            return;
        }
        
	}
	else {
		
		[self reset];
	}
}

- (void)reset {
    //NSString* noDistance = [self.delegate formatDistance:0];
	NSLog(@"Resetting current run");
	[self.delegate stop];
	[self.delegate reset];
	//[self.runTimeLabel setText:DEFAULT_TIMER_TEXT];
	//[self.distanceLabel setText:noDistance];
	//[self.altitudeLabel setText:noDistance];
	//[self.avgSpeedLabel setText:@"-"];
	//[self.curSpeedLabel setText:@"-"];
	[self setStartButtonTitle:START_BUTTON_TEXT];
	self.started = NO;
	[self setStartButtonTitle:START_BUTTON_TEXT];
}

-(void)setStartButtonTitle:(NSString*)text {
	[self.startButton setTitle:text forState:UIControlStateNormal];
}


-(void)updateRunDisplay {
	
	// Total distance formatting
	NSString* dist = [self.delegate formatDistance:self.delegate.totalDistance];
	
	// Altitude
	NSString* alt = [self.delegate formatDistanceBasic:self.delegate.altitude];
	
	// Average speed
	NSString* avgSpeed = [self.delegate formatSpeed:self.delegate.avgSpeed];
	
	// Current speed
	NSString* curSpeed = [self.delegate formatSpeed:self.delegate.currentSpeed];
	
	NSLog(@"New distance: %@", dist);
	NSLog(@"New altitude: %@", alt);
	NSLog(@"New avg. speed: %@", avgSpeed);
	NSLog(@"New speed: %@", curSpeed);
	
	[self.distanceLabel setText:dist];
	[self.altitudeLabel setText:alt];
	[self.avgSpeedLabel setText:avgSpeed];
	[self.curSpeedLabel setText:curSpeed];
	
}


-(void)drawLineForLocations:(CLLocation*)location fromLocation:(CLLocation*)oldLocation {
    
	MKMapPoint* points = malloc(sizeof(CLLocationCoordinate2D) * 2);
	points[0] = MKMapPointForCoordinate(oldLocation.coordinate);
	points[1] = MKMapPointForCoordinate(location.coordinate);
	
	MKPolyline* line = [[MKPolyline polylineWithPoints:points count:2] autorelease];
	
	[self.mapView addOverlay:line];
	free(points);
}

- (void)annotateMap:(CLLocation*)location {
	MKPointAnnotation* annotation = [MKPointAnnotation alloc];
	annotation.coordinate = location.coordinate;
	[self.mapView addAnnotation:annotation];
}

-(void)addAnnotation:(MKPointAnnotation*)annotation {
    [self.mapView addAnnotation:annotation];
}

#pragma mark - Métodos de QBKOverlayMenuViewDelegate
- (void)overlayMenuView:(QBKOverlayMenuView *)overlayMenuView didActivateAdditionalButtonWithIndex:(NSInteger)index
{
    //NSLog(@"Botón pulsado con índice: %d", index);
    
    self.mapView.mapType = index;
    
    [self.delegate saveMapType:self.mapView];
}

-(void)didActivateMyLocation:(QBKOverlayMenuView *)overlayMenuView
{
    [self.mapView showsUserLocation];
}

- (void)didPerformUnfoldActionInOverlayMenuView:(QBKOverlayMenuView *)overlaymenuView
{
    //NSLog(@"Menú DESPLEGADO");
}

- (void)didPerformFoldActionInOverlayMenuView:(QBKOverlayMenuView *)overlaymenuView
{
    //NSLog(@"Menú REPLEGADO");
}

@end

@implementation StatsViewController (MKMapViewDelegate)

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id)overlay {
	
	MKPolylineView* overlayView = [[[MKPolylineView alloc] initWithPolyline:(MKPolyline*)overlay] autorelease];
	overlayView.strokeColor = [UIColor blueColor];
    overlayView.lineWidth = 5.f;
	
	return overlayView;
	
}

- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation
{
    static NSString *AnnotationViewID = @"annotationViewID";
    
    MKAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
    if (annotationView == nil)
    {
        annotationView = [[[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID] autorelease];
    }
    
    annotationView.annotation = annotation;
    
    if ([annotationView.annotation isKindOfClass:[StartPointAnnotation class]]) {
        annotationView.image = [UIImage imageNamed:@"start.png"];
    }
    else if ([annotationView.annotation isKindOfClass:[EndPointAnnotation class]]) {
        annotationView.image = [UIImage imageNamed:@"end.png"];
    }
    
    return annotationView;
}

@end
