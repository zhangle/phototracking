//
//  TrackPoint.m
//  GPSLogger
//
//  Created by NextBusinessSystem on 12/01/26.
//  Copyright (c) 2012 NextBusinessSystem Co., Ltd. All rights reserved.
//

#import "TrackPoint.h"
#import "Track.h"
#import "JSONKit.h"

@implementation TrackPoint

@synthesize longitude;
@synthesize latitude;
@synthesize timestamp;
@synthesize altitude;
@synthesize track;


- (CLLocationCoordinate2D)coordinate
{
    return CLLocationCoordinate2DMake(self.latitude.floatValue, self.longitude.floatValue);
}

- (void)setPointsFromJSON:(NSString*)location
{
    NSDictionary *deserializedData = [location objectFromJSONString];
    
    NSLog(@"TrackPoint %@", [deserializedData description]);
    
    self.altitude = [deserializedData objectForKey:@"altitude"];
    self.latitude = [[deserializedData objectForKey:@"coordinate"] objectForKey:@"latitude"];
    self.longitude = [[deserializedData objectForKey:@"coordinate"] objectForKey:@"longitude"];
    
    NSNumber *time = [deserializedData objectForKey:@"timestamp"];
    
    self.timestamp = [[[NSDate alloc] initWithTimeIntervalSince1970:[time intValue]] autorelease];
}

@end
