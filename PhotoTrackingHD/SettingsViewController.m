//
//  SecondViewController.m
//  PhotoTrackingHD
//
//  Created by 张 乐 on 12-8-20.
//  Copyright (c) 2012年 张 乐. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController
@synthesize sensitivityLabel;
@synthesize sensitivitySlider;
@synthesize metricSwitch;
@synthesize backgroundSwitch;
@synthesize backgroundNotSupportedLabel;

- (void)viewDidLoad
{
    self.delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	
	[self.sensitivityLabel setText:[self.delegate formatDistance:self.delegate.sensitivity]];
	[self.metricSwitch setOn:self.delegate.isMetric];
    [self.backgroundSwitch setOn:self.delegate.isBackground];
    
    // check device supported
    if([self.delegate isSupportBackgroundTask]){
        // disable the background switch
        [self.backgroundSwitch setEnabled:YES];
        [self.backgroundNotSupportedLabel setHidden:YES];
    }
    else {
        [self.backgroundSwitch setEnabled:NO];
        [self.backgroundNotSupportedLabel setHidden:NO];
    }

    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [self setSensitivityLabel:nil];
    [self setSensitivitySlider:nil];
    [self setMetricSwitch:nil];
    [self setBackgroundSwitch:nil];
    [self setBackgroundNotSupportedLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)dealloc {
    [sensitivityLabel release];
    [sensitivitySlider release];
    [metricSwitch release];
    [backgroundSwitch release];
    [backgroundNotSupportedLabel release];
    [super dealloc];
}
- (IBAction)updateSensitivity {
    double value = [self.delegate updateSensitivity];
	NSString* formattedSensitivityValue = [self.delegate formatDistance:value];
	[self.sensitivityLabel setText:formattedSensitivityValue];
}

- (IBAction)changeUnitSystem {
    [self.delegate updateUnitSystem];
	[self updateSensitivity];
}

- (IBAction)changeBackground {
    [self.delegate updateBackground];
}
@end
