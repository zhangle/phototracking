//
//  TrackViewController.m
//  PhotoTrackingHD
//
//  Created by 张 乐 on 12-8-20.
//  Copyright (c) 2012年 张 乐. All rights reserved.
//

#import "HistoryController.h"
#import "Track.h"
#import "config.h"
#import "MapViewController.h"

@interface HistoryController ()

@end

@interface HistoryController (UIAlertViewDelegate) <UIAlertViewDelegate>
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
@end

@implementation HistoryController (UIAlertViewDelegate)
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != alertView.cancelButtonIndex && self.deleteIndexPath) {
        
        NSLog(@"delete %d", self.deleteIndexPath.row);
        Track *track = [self.tracks objectAtIndex:self.deleteIndexPath.row];
        
        [self.tracks removeObject:track];
        
        [self.delegate deleteTrack:track.track_id];
        
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:self.deleteIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        self.deleteIndexPath = nil;
    }
}
@end

@implementation HistoryController

@synthesize delegate;
@synthesize tracks;
@synthesize deleteIndexPath;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [self setSearchBar:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    [self.tracks release];
    self.tracks = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView setContentOffset:CGPointMake(0, 88)];
    
    self.tracks = [[self.delegate historyList] mutableCopy];
    
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.tracks release];
    self.tracks = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"PushMapViewController"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        Track *track = [self.tracks objectAtIndex:indexPath.row];
        
        MapViewController *viewController = (MapViewController *)segue.destinationViewController;
        viewController.track = track;
        viewController.trackViewController = self;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tracks count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    Track *track = [self.tracks objectAtIndex:indexPath.row];
    
    /*
     NSDateFormatter *formatter = [NSDateFormatter new];
     formatter.dateFormat = @"yyyy-MM-dd hh:mm:ss";
     NSString *text = [formatter stringFromDate:track.timestamp];
     */
    
    cell.textLabel.text = track.name;
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Track *track = [self.tracks objectAtIndex:indexPath.row];
        
        self.deleteIndexPath = indexPath;
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Delete", nil) message:[NSString stringWithFormat:NSLocalizedString(@"Delete the track named", nil), track.name]  delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"OK",nil), nil];
        
        alert.alertViewStyle = UIAlertViewStyleDefault;
        [alert show];
        [alert release];
        
        /*
        [self.delegate confirmDeleteTrack:track.track_id];
        
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
         */
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

- (void)dealloc {
    [_searchBar release];
    [super dealloc];
}

#pragma mark - UISearchBarDelegate delegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (!searchText || [searchText length] <= 0) {
        [self.tracks removeAllObjects];
        [self.tracks addObjectsFromArray:[self.delegate historyList]];
    }
    else {
        NSMutableArray *searchedTracks = [[NSMutableArray alloc] init];
        
        for (Track *track in [self.delegate historyList]) {
            if ([track.name rangeOfString:searchText].location != NSNotFound) {
                [searchedTracks addObject:track];
            }
        }
        
        [self.tracks removeAllObjects];
        [self.tracks addObjectsFromArray:searchedTracks];
        
        [searchedTracks removeAllObjects];
        [searchedTracks release];
    }
    
    [self.tableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope
{
    if (selectedScope == 0) {
        NSArray *sortedTracks = [self.tracks sortedArrayUsingComparator: ^(Track* track1, Track* track2) {
            return [track2.timestamp compare:track1.timestamp];
        }];
        
        [self.tracks removeAllObjects];
        [self.tracks addObjectsFromArray:sortedTracks];
    }
    else if (selectedScope == 1) {
        NSArray *sortedTracks = [self.tracks sortedArrayUsingComparator: ^(Track* track1, Track* track2) {
            return [track1.name compare:track2.name];
        }];
        
        [self.tracks removeAllObjects];
        [self.tracks addObjectsFromArray:sortedTracks];
    }
    
    [self.tableView reloadData];
}

@end
