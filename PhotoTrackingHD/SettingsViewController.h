//
//  SecondViewController.h
//  PhotoTrackingHD
//
//  Created by 张 乐 on 12-8-20.
//  Copyright (c) 2012年 张 乐. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@class AppDelegate;

@interface SettingsViewController : UIViewController

@property (nonatomic, retain) AppDelegate* delegate;

@property (retain, nonatomic) IBOutlet UILabel *sensitivityLabel;
@property (retain, nonatomic) IBOutlet UISlider *sensitivitySlider;
@property (retain, nonatomic) IBOutlet UISwitch *metricSwitch;
@property (retain, nonatomic) IBOutlet UISwitch *backgroundSwitch;
@property (retain, nonatomic) IBOutlet UITextView *backgroundNotSupportedLabel;

- (IBAction)updateSensitivity;
- (IBAction)changeUnitSystem;
- (IBAction)changeBackground;

@end
