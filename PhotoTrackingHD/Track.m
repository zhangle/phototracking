//
//  Track.m
//  GPSLogger
//
//  Created by NextBusinessSystem on 12/01/26.
//  Copyright (c) 2012 NextBusinessSystem Co., Ltd. All rights reserved.
//

#import "Track.h"
#import "TrackPoint.h"


@implementation Track

@synthesize track_id;
@synthesize name;
@synthesize timestamp;
@synthesize distance;
@synthesize elapsedTime;
@synthesize trackpoints;

- (id)init {
    self = [super init];
    self.trackpoints = [[[NSMutableSet alloc] init] autorelease];
    return self;
}

- (void)addTrackpointsObject:(TrackPoint *)value {
    [self.trackpoints addObject:value];
}

- (NSArray *)sotredTrackPoints
{
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"timestamp" ascending:YES];
    
    NSArray* points = [self.trackpoints sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
    
    [descriptor release];
    descriptor = nil;
    
    return points;
}

@end
