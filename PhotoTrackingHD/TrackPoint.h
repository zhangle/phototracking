//
//  TrackPoint.h
//  GPSLogger
//
//  Created by NextBusinessSystem on 12/01/26.
//  Copyright (c) 2012 NextBusinessSystem Co., Ltd. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
@class Track;

@interface TrackPoint: NSObject {
    NSNumber * longitude;
    NSNumber * latitude;
    NSDate * timestamp;
    NSNumber * altitude;
    Track *track;
}

@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSDate * timestamp;
@property (nonatomic, retain) NSNumber * altitude;
@property (nonatomic, retain) Track *track;

- (CLLocationCoordinate2D)coordinate;

- (void)setPointsFromJSON:(NSString*)location;

@end
