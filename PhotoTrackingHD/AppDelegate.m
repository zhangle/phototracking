//
//  AppDelegate.m
//  PhotoTrackingHD
//
//  Created by 张 乐 on 12-8-20.
//  Copyright (c) 2012年 张 乐. All rights reserved.
//

#import "AppDelegate.h"
#import "StatsViewController.h"
#import "SettingsViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import <GPX/GPX.h>

#import "Track.h"
#import "TrackPoint.h"
#import "StartPointAnnotation.h"
#import "EndPointAnnotation.h"

@implementation AppDelegate

static const char* createDBTables = "\
CREATE TABLE tracks (\
'id' INTEGER PRIMARY KEY AUTOINCREMENT, \
'name' TEXT, \
'elapsedTime' INTEGER, \
'distance' INTEGER, \
'timestamp' INTEGER \
);\
CREATE TABLE locations (\
'id' INTEGER PRIMARY KEY AUTOINCREMENT, \
'track_id' INTEGER, \
'location' TEXT, \
'timestamp' INTEGER \
);";

static const char* trackInsertDB = "INSERT INTO tracks (\"name\",\"elapsedTime\",\"distance\",\"timestamp\") VALUES(?, ?, ?, ?);";

static const char* locationInsertDB = "INSERT INTO locations (\"track_id\",\"location\",\"timestamp\") VALUES(?, ?, ?);";

static const char* allTracksDB = "SELECT * FROM tracks ORDER BY timestamp DESC;";
static const char* getTrackDB = "SELECT * FROM tracks WHERE id = ?;";

static const char* getLocationsDB = "SELECT * FROM locations WHERE track_id = ?;";

static const char* deleteTrackDB = "DELETE FROM tracks WHERE id = ?;";

static const char* renameTrackDB = "UPDATE tracks SET name = ? WHERE id = ?;";

static const char* deleteLocationsDB = "DELETE FROM locations WHERE track_id = ?;";

@synthesize tabBarController;
@synthesize statsController;
@synthesize settingsController;

@synthesize locationManager;
@synthesize totalDistance;
@synthesize avgSpeed;
@synthesize currentSpeed;
@synthesize altitude;
@synthesize locationPoints;
@synthesize startTime;
@synthesize elapsedTime;
@synthesize timer;
@synthesize isMetric;
@synthesize sensitivity;
@synthesize hasZoomedOnMap;
@synthesize bottomLeft;
@synthesize topRight;

@synthesize isBackground;
@synthesize isRunning;

- (void)dealloc
{
    [_window release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self initLocationsDB];
    
    self.tabBarController = (UITabBarController *)self.window.rootViewController;
    
    self.statsController = (StatsViewController*)[self.tabBarController.viewControllers objectAtIndex:0];
	self.settingsController = (SettingsViewController*)[self.tabBarController.viewControllers objectAtIndex:2];
	    
	// Initialize stats
	[self reset];
    
    self.lastLocationIndex = 0;
	
    [self checkLocationServiceSupported];
    
    // Initialize location tracking
    self.locationManager = [[[CLLocationManager alloc] init] autorelease];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = MIN_DIST_CHANGE;
    self.locationManager.desiredAccuracy = DEFAULT_PRECISION;
    self.locationManager.distanceFilter = self.sensitivity > 0 ? self.sensitivity : DEFAULT_PRECISION;
    
    // Tab Bar
	self.tabBarController.delegate = self;
	
	// Initialize map view
	self.statsController.mapView.showsUserLocation = YES;
	self.hasZoomedOnMap = NO;
	
    return YES;

}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    if([self isSupportBackgroundTask]){
        self.bgTask = [application beginBackgroundTaskWithExpirationHandler:^{
            // Clean up any unfinished task business by marking where you.
            // stopped or ending the task outright.
            [application endBackgroundTask:self.bgTask];
            self.bgTask = UIBackgroundTaskInvalid;
        }];
        
        // Start the long-running task and return immediately.
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSLog(@"backgroud task isRunning %d %d", self.isRunning, self.isBackground);
            
            if (self.isRunning && self.isBackground) {
                self.lastLocationIndex = [self.locationPoints count];
            }
            else {
                
                [application endBackgroundTask:self.bgTask];
                self.bgTask = UIBackgroundTaskInvalid;
            }
        });
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    [application endBackgroundTask:self.bgTask];
    self.bgTask = UIBackgroundTaskInvalid;
    
    [self restoreUnShownLocations];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self.locationPoints release];
}


- (void)loadMapType:(MKMapView*)mapView
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *mapType = [defaults objectForKey:@"map-type"];
    if (!mapType) {
        mapType = [NSNumber numberWithInteger:0];
    }
    
    mapView.mapType = [mapType integerValue];
}

- (void)saveMapType:(MKMapView*)mapView
{
    NSNumber *mapType = [NSNumber numberWithInteger:mapView.mapType];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:mapType forKey:@"map-type"];
    [defaults synchronize];
}

#pragma mark -
#pragma mark Custom methods

-(void)clearMap {
    
	MKMapView* map = self.statsController.mapView;
	
	// Clear map annotations
    if (map.annotations != NULL) {
        for (id annotation in map.annotations) {
            if (![annotation isKindOfClass:[MKUserLocation class]]){
                [map removeAnnotation:annotation];
            }
        }
    }
    
	// Hide map overlays
    if (map.overlays != NULL) {
        for (id overlay in map.overlays) {
            MKOverlayView* overlayView = [map viewForOverlay:overlay];
            overlayView.hidden = YES;
            [overlayView setNeedsDisplay];
        }
    }
}

- (void)reset {
	if (self.locationPoints != NULL) {
        [self saveLocations];
	}
    
	self.startTime = NULL;
	self.elapsedTime = 0;
	self.totalDistance = 0;
	self.avgSpeed = 0;
	self.altitude = 0;
	
	self.locationPoints = [[[NSMutableArray alloc] initWithCapacity:DEFAULT_NUM_POINTS] autorelease];
	
	NSLog(@"Reset stats values");
	
}

-(void)start {
    if (self.startTime == NULL) {
		self.startTime = [NSDate date];
	}
    
    [self clearMap];
    
    self.statsController.mapView.showsUserLocation = NO;
    
    [self.locationManager startUpdatingLocation];
	[self.locationManager startMonitoringSignificantLocationChanges];

	self.timer = [NSTimer scheduledTimerWithTimeInterval:TIMER_PERIOD
												  target:self
												selector:@selector(updateTimer)
												userInfo:nil
                                                 repeats:YES];
    self.isRunning = YES;
    [self saveSettings];
    
	NSLog(@"Started tracking");
}


-(void)stop {
	self.elapsedTime = [self getElapsedTimeInMilliseconds];
	
    self.isRunning = NO;
    [self saveSettings];
    
	[self.timer invalidate];
	[self.locationManager stopUpdatingLocation];
    [self.locationManager stopMonitoringSignificantLocationChanges];
	
	// Add breakpoint
	if ([self.locationPoints count] > 1) {
		CLLocation* lastLocation = [self.locationPoints objectAtIndex:[self.locationPoints count]-1];
		//[self.statsController annotateMap:lastLocation];
        
        EndPointAnnotation * annotation = [[[EndPointAnnotation alloc] init] autorelease];
        annotation.coordinate =  lastLocation.coordinate;
        [self.statsController addAnnotation:annotation];
	}
	
	NSLog(@"Stopped tracking");
	
}

-(void)simulateFreewayLocation {
    int delta_milli = [self getElapsedTimeInMilliseconds];
	
	int hours = delta_milli / (3600 * 1000);
	int minutes = (delta_milli - (3600 * 1000) * hours) / (60 * 1000);
	int seconds = (delta_milli - (3600 * 1000) * hours - (60 * 1000) * minutes) / 1000;
	int ms = (delta_milli - (3600 * 1000) * hours - (60 * 1000) * minutes - 1000 * seconds) / 100;
    
	// Simulate location change every 2 seconds
	if (seconds % 2 == 0 && ms == 0) {
		
		BOOL isFirst = ([self.locationPoints count] == 0);
		
		NSLog(@"Simulating location change");
		
		CLLocation* oldLocation = NULL;
		if (isFirst == NO) {
			oldLocation = [[self locationPoints] objectAtIndex:[self.locationPoints count] - 1];
		}
		
		CLLocationCoordinate2D newCoords;
		if (isFirst == YES) {
			newCoords.latitude = 37.0;
			newCoords.longitude = 122.0;
		} else {
			CLLocationCoordinate2D oldCoords = oldLocation.coordinate;
			newCoords.latitude = oldCoords.latitude + [self generateRamdonChange];
			newCoords.longitude = oldCoords.longitude + [self generateRamdonChange];
		}
		
        CLLocation* newLocation = [[[CLLocation alloc] initWithCoordinate:newCoords altitude:10 horizontalAccuracy:10 verticalAccuracy:10 timestamp:[NSDate date]] autorelease];
		
		[self processLocationChange:newLocation fromLocation:oldLocation];
        
	}
}

-(void)updateTimer {
	
	int delta_milli = [self getElapsedTimeInMilliseconds];
	
	int hours = delta_milli / (3600 * 1000);
	int minutes = (delta_milli - (3600 * 1000) * hours) / (60 * 1000);
	int seconds = (delta_milli - (3600 * 1000) * hours - (60 * 1000) * minutes) / 1000;
	int ms = (delta_milli - (3600 * 1000) * hours - (60 * 1000) * minutes - 1000 * seconds) / 100;
	
	NSString* timeString = [NSString stringWithFormat:@"%.2d:%.2d:%.2d.%d", hours, minutes, seconds, ms];
	
	[self.statsController.runTimeLabel setText:timeString];
}


-(double)generateRamdonChange {
	return (double) ((rand() / (double)RAND_MAX) / 300.0);
}


-(void)updateMap:(CLLocation*)oldLocation newLocation:(CLLocation*)location {
	
	MKCoordinateRegion region;
	
	MKCoordinateSpan span;
	double scalingFactor = ABS(cos(2 * M_PI * location.coordinate.latitude / 360.0));
	
	if (self.hasZoomedOnMap == NO) {
		
		// Initialize the zoom level
		span.latitudeDelta = MAP_RADIUS / 69.0;
		span.longitudeDelta = MAP_RADIUS / (scalingFactor * 69.0);
		region.span = span;
		region.center = location.coordinate;
		
		self.hasZoomedOnMap = YES;
		
	} else {
		
		// Update the region but conserve the zoom level
		region = self.statsController.mapView.region;
		region.center = location.coordinate;
		
		// Check if the extremities of the path are outside the current region
		if (self.bottomLeft.latitude < region.center.latitude + region.span.latitudeDelta ||
			self.bottomLeft.longitude < region.center.longitude - region.span.longitudeDelta ||
			self.topRight.latitude > region.center.latitude - region.span.latitudeDelta ||
			self.topRight.longitude > region.center.longitude + region.span.longitudeDelta) {
			
			NSLog(@"Resizing visible region");
			
			double lat_span = self.topRight.latitude - self.bottomLeft.latitude;
			double lng_span = self.topRight.longitude - self.bottomLeft.longitude;
			region.center.latitude = self.bottomLeft.latitude + lat_span / 2;
			region.center.longitude = self.bottomLeft.longitude + lng_span / 2;
			region.span.latitudeDelta = lat_span * 1.25;
			region.span.longitudeDelta = lng_span * 1.25;
			
		}
		
	}
    
    //[self.statsController.mapView setCenterCoordinate:location.coordinate animated:YES];
    NSLog(@"setRegion %f %f %f %f", region.center.latitude, region.center.longitude, region.span.latitudeDelta, region.span.longitudeDelta);
    
	[self.statsController.mapView setRegion:region];
	
	if (oldLocation != location) {
        
		// Draw a line between the old and new locations
		[self.statsController drawLineForLocations:location fromLocation:oldLocation];
		
	} else {
		
		// Starting point
		[self.statsController annotateMap:location];
		
	}
	
	NSLog(@"Updated map");
	
}

-(double)updateSensitivity {
	
	double sensitivityRatio = [self.settingsController.sensitivitySlider value];
	int range = MAX_DIST_CHANGE - MIN_DIST_CHANGE;
	self.sensitivity = MIN_DIST_CHANGE + range * sensitivityRatio;
	NSLog(@"Changed sensitivity to %f", self.sensitivity);
	
	[self saveSettings];
	self.locationManager.distanceFilter = self.sensitivity;
	
	
	return self.sensitivity;
}


-(BOOL)updateUnitSystem {
	
	self.isMetric = [self.settingsController.metricSwitch isOn];
	NSLog(@"Is metric = %d", self.isMetric);
	
	[self saveSettings];
	
	return self.isMetric;
}


-(BOOL)updateBackground {
	
	self.isBackground = [self.settingsController.backgroundSwitch isOn];
	NSLog(@"Is background = %d", self.isBackground);
	
	[self saveSettings];
	
	return self.isBackground;
}

-(NSString*)configFilePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *libraryDirectory = [paths objectAtIndex:0];
    
    NSString *config =  [NSString stringWithFormat:@"%@/%@", libraryDirectory, SETTINGS_FILE];
    
    return config;
}

-(NSString*)dbFilePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *libraryDirectory = [paths objectAtIndex:0];
    
    NSString *dbPath =  [NSString stringWithFormat:@"%@/%@", libraryDirectory, DB_PATH];
    
    return dbPath;
}

-(NSString*)gpxFilePath:(NSString *)file {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    NSString *gpxFile =  [NSString stringWithFormat:@"%@/%@.gpx", documentDirectory, file];
    
    return gpxFile;
}

-(BOOL)isRunBackground {
    return self.bgTask != UIBackgroundTaskInvalid;
}

-(BOOL)isSupportBackgroundTask {
    UIDevice* device = [UIDevice currentDevice];
    BOOL backgroundSupported = NO;
    if ([device respondsToSelector:@selector(isMultitaskingSupported)])
        backgroundSupported = device.multitaskingSupported;
    
    return backgroundSupported;
}

-(BOOL)checkLocationServiceSupported {
    if (![CLLocationManager locationServicesEnabled]) {
        
        // alert user
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location service not enabled!"
                                                        message:@"You must enable the location service in setting to use this app."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        return NO;
    }
    else {
        return YES;
    }
}

-(void)saveLocations {
    
    if ([self.locationPoints count] > 1) {
        ObjSqliteStatement* statement = nil;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeStyle:NSDateFormatterFullStyle];
        [dateFormatter setDateFormat:@"yyyy.MM.dd.HH.mm"];
        
        NSString* startDateTime = [dateFormatter stringFromDate:self.startTime];
        NSString* endDateTime = [dateFormatter stringFromDate:[[[NSDate alloc]initWithTimeInterval:self.elapsedTime sinceDate:self.startTime] autorelease]];
        
        NSLog(@"startDateTime %@", startDateTime);
        NSLog(@"endDateTime %@", endDateTime);
        
        NSString* trackName = [NSString stringWithFormat:@"%@", startDateTime];
        
        [self createGPX:trackName];
        
        NSLog(@"trackName %@", trackName);
        
        statement = [[ObjSqliteStatement alloc] initWithSQL:trackInsertDB db:self.db];
        [statement bindText:trackName toColumn:1];
        [statement bindInt:self.elapsedTime toColumn:2];
        [statement bindInt:self.totalDistance toColumn:3];
        [statement bindInt:[self.startTime timeIntervalSince1970] toColumn:4];
        
        BOOL ret = [statement step];
        if (!ret) {
            NSLog(@"statement error %@", self.db.lastErrorMessage);
            [statement release];
            [dateFormatter release];
            
            return;
        }
        
        int trackID = [statement lastInsertRowID];
        NSLog(@"trackID %d", trackID);
        
        [statement release];
        statement = nil;
        
        statement = [[ObjSqliteStatement alloc] initWithSQL:locationInsertDB db:self.db];
        
        for (int i = 0; i < [self.locationPoints count]; i++) {
            CLLocation* location =[self.locationPoints objectAtIndex:i];
            
            NSLog(@"restore unshown location");
            
            NSString* json = [NSString stringWithFormat:@"{\
                              \"altitude\":%f,\
                              \"coordinate\":{\
                              \"latitude\":%f,\
                              \"longitude\":%f\
                              },\
                              \"horizontalAccuracy\":%f,\
                              \"verticalAccuracy\":%f,\
                              \"timestamp\":%f,\
                              \"speed\":%f,\
                              \"course\":%f\
                              }", location.altitude, location.coordinate.latitude, location.coordinate.longitude, location.horizontalAccuracy, location.verticalAccuracy, [location.timestamp timeIntervalSince1970], location.speed, location.course];
            
            NSLog(@"save location %@ to db", json);
            
            [statement bindInt:trackID toColumn:1];
            [statement bindText:json toColumn:2];
            [statement bindInt:[[location timestamp] timeIntervalSince1970] toColumn:3];
            BOOL ret = [statement stepAndResetStatement];
            if (!ret) {
                NSLog(@"statement error %@", self.db.lastErrorMessage);
            }
        }
        
        [statement release];
        statement = nil;
        
        [dateFormatter release];
        dateFormatter = nil;
    }
}

-(void)initLocationsDB {
    self.db = [[[ObjSqliteDB alloc] initWithPath:[self dbFilePath]] autorelease];
    
    self.db.createSQL = createDBTables;
    [self.db sqliteDB];
    
    BOOL dbExist = [self.db dbExists];
    
    NSLog(@"db isExist %d", dbExist);
}

-(void)restoreUnShownLocations {
    // Update map view
    if (self.lastLocationIndex > 0) {
        for (int i = self.lastLocationIndex; i < [self.locationPoints count]; i++) {
            CLLocation* lastKnownLocation = [self.locationPoints objectAtIndex:i-1];
            CLLocation* newLocation =[self.locationPoints objectAtIndex:i];
            
            NSLog(@"restore unshown location");
            
            [self updateMap:lastKnownLocation newLocation:newLocation];
        }
        
        self.lastLocationIndex = 0;
    }
}

-(void)saveSettings {
	
	NSString* data = [NSString stringWithFormat:@"%f%@%d%@%d%@%d", self.sensitivity, SETTINGS_SEP, self.isMetric, SETTINGS_SEP, self.isBackground, SETTINGS_SEP, self.isRunning];
	NSLog(@"settings data = %@", data);
    
    NSError* theError = nil;
    
	if ([data writeToFile:[self configFilePath] atomically:NO encoding:NSASCIIStringEncoding error:&theError] == YES) {
		NSLog(@"Saved new settings");
	}
	else {
		NSLog(@"Could not saved settings, %@", theError);
	}
}


-(BOOL)loadSettings {
    
	BOOL didLoad = NO;
	
	NSString* data = [[NSString alloc] initWithContentsOfFile:[self configFilePath] encoding:NSASCIIStringEncoding error:nil];
	NSArray* parts = [data componentsSeparatedByString:SETTINGS_SEP];
	NSLog(@"settings data = %@", parts);
	if ([parts count] == 4) {
		
		// Parse settings values
		self.sensitivity = [[parts objectAtIndex:0] doubleValue];
		self.isMetric = [[parts objectAtIndex:1] boolValue];
        self.isBackground = [[parts objectAtIndex:2] boolValue];
        self.isRunning = [[parts objectAtIndex:3] boolValue];
		NSLog(@"Loaded settings");
		
		// Update settings display
		[self.settingsController.sensitivityLabel setText:[self formatDistance:self.sensitivity]];
		[self.settingsController.metricSwitch setOn:self.isMetric];
        [self.settingsController.backgroundSwitch setOn:self.isBackground];
		
		didLoad = YES;
		
	} else {
		
		// No saved settings available
		NSLog(@"No saved settings found");
		
	}
	[data release];
    data = nil;
	
	return didLoad;
}

-(NSMutableArray*)historyList {
    NSMutableArray* list = [[[NSMutableArray alloc] init] autorelease];
    
    ObjSqliteStatement* statement = [[ObjSqliteStatement alloc] initWithSQL:allTracksDB db:self.db];
    
    while ([statement stepAndHasNextRow]) {
        int trackID = [statement intFromColumn:0];
        NSString* trackName = [statement textFromColumn:1];
        int trackElapsedTime = [statement intFromColumn:2];
        int trackDistance = [statement intFromColumn:3];
        int trackTimeStamp = [statement intFromColumn:4];
        
        NSLog(@"history %d %@ %d %d %d", trackID, trackName, trackElapsedTime, trackDistance, trackTimeStamp);
        
        Track* track = [[Track alloc] init];
        
        track.track_id = [NSString stringWithFormat:@"%d", trackID];
        track.timestamp = [[[NSDate alloc] initWithTimeIntervalSince1970: trackTimeStamp] autorelease];
        track.distance = trackDistance;
        track.elapsedTime = trackElapsedTime;
        track.name = trackName;
        
        [list addObject:track];
        
        [track release];
    }
    
    [statement release];
    statement = nil;
    
    return list;
}

-(NSMutableSet*)getTrackLocations:(NSString*)trackID {
    NSMutableSet* trackpoints = [[[NSMutableSet alloc] init] autorelease];
    
    ObjSqliteStatement* statement = [[ObjSqliteStatement alloc] initWithSQL:getLocationsDB db:self.db];
    [statement bindText:trackID toColumn:1];
    
    while ([statement stepAndHasNextRow]) {
        int locationID = [statement intFromColumn:0];
        NSString* location = [statement textFromColumn:2];
        int timeStamp = [statement intFromColumn:3];
        
        NSLog(@"history %d %@ %d", locationID, location, timeStamp);
        
        TrackPoint* point = [[TrackPoint alloc] init];
        
        point.timestamp = [[[NSDate alloc] initWithTimeIntervalSince1970: timeStamp] autorelease];
        
        [point setPointsFromJSON:location];
        
        [trackpoints addObject:point];
        
        [point release];
    }
    
    [statement release];
    statement = nil;
    
    return trackpoints;
}


-(void)renameTrack:(NSString*)trackID trackName:(NSString*)trackName {
    ObjSqliteStatement* statement = [[ObjSqliteStatement alloc] initWithSQL:getTrackDB db:self.db];
    [statement bindText:trackID toColumn:1];
    BOOL ret = [statement step];
    
    if (!ret) {
        NSLog(@"renameTrack get track error %@", self.db.lastErrorMessage);
        [statement release];
        return;
    }
    
    NSString* origTrackName = [statement textFromColumn:1];
    
    [statement release];
    statement = nil;
    
    statement = [[ObjSqliteStatement alloc] initWithSQL:renameTrackDB db:self.db];
    [statement bindText:trackName toColumn:1];
    [statement bindText:trackID toColumn:2];
    ret = [statement step];
    
    if (!ret) {
        NSLog(@"renameTrack delete track error %@", self.db.lastErrorMessage);
    }
    
    [statement release];
    statement = nil;
    
    NSString *filePath = [self gpxFilePath:origTrackName];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    BOOL fileExists = [fileManager fileExistsAtPath:filePath];
    NSLog(@"Path to file: %@", filePath);
    NSLog(@"File exists: %d", fileExists);
    NSLog(@"Is deletable file at path: %d", [fileManager isDeletableFileAtPath:filePath]);
    if (fileExists)
    {
        BOOL success = [fileManager copyItemAtPath:filePath toPath:[self gpxFilePath:trackName] error:&error];
        if (!success) NSLog(@"Error: %@", [error localizedDescription]);
        success = [fileManager removeItemAtPath:filePath error:&error];
        if (!success) NSLog(@"Error: %@", [error localizedDescription]);
    }

}

-(void)deleteTrack:(NSString*)track_id {
    ObjSqliteStatement* statement = [[ObjSqliteStatement alloc] initWithSQL:getTrackDB db:self.db];
    [statement bindText:track_id toColumn:1];
    BOOL ret = [statement step];
    
    if (!ret) {
        NSLog(@"deleteTrack get track error %@", self.db.lastErrorMessage);
        [statement release];
        
        return;
    }
    
    NSString* trackName = [statement textFromColumn:1];
    
    [statement release];
    statement = nil;
    statement = [[ObjSqliteStatement alloc] initWithSQL:deleteTrackDB db:self.db];
    [statement bindText:track_id toColumn:1];
    ret = [statement step];
    
    if (!ret) {
        NSLog(@"deleteTrack delete track error %@", self.db.lastErrorMessage);
    }
    
    [statement release];
    statement = nil;
    statement = [[ObjSqliteStatement alloc] initWithSQL:deleteLocationsDB db:self.db];
    [statement bindText:track_id toColumn:1];
    ret = [statement step];
    
    if (!ret) {
        NSLog(@"deleteTrack delete locations error %@", self.db.lastErrorMessage);
    }
    
    [statement release];
    statement = nil;
    
    NSString *filePath = [self gpxFilePath:trackName];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    BOOL fileExists = [fileManager fileExistsAtPath:filePath];
    NSLog(@"Path to file: %@", filePath);
    NSLog(@"File exists: %d", fileExists);
    NSLog(@"Is deletable file at path: %d", [fileManager isDeletableFileAtPath:filePath]);
    if (fileExists)
    {
        BOOL success = [fileManager removeItemAtPath:filePath error:&error];
        if (!success) NSLog(@"Error: %@", [error localizedDescription]);
    }
}


- (NSString *)createGPX:(NSString *)file {
    // gpx
    GPXRoot *gpx = [GPXRoot rootWithCreator:@"GPSLogger"];
    
    // gpx > trk
    GPXTrack *gpxTrack = [gpx newTrack];
    gpxTrack.name = [file retain];
    
    // gpx > trk > trkseg > trkpt
    for (CLLocation *trackPoint in self.locationPoints) {
        GPXTrackPoint *gpxTrackPoint = [gpxTrack newTrackpointWithLatitude:trackPoint.coordinate.latitude longitude:trackPoint.coordinate.longitude];
        gpxTrackPoint.elevation = trackPoint.altitude;
        gpxTrackPoint.time = trackPoint.timestamp;
    }
    
    NSString *gpxString = [gpx.gpx retain];
    
    // write gpx to file
    NSError *error;
    NSString *filePath = [self gpxFilePath:file];
    if (![gpxString writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        if (error) {
            NSLog(@"error, %@", error);
        }
        
        [gpxString release];
        [gpxTrack release];
        
        return nil;
    }
    
    [gpxString release];
    [gpxTrack release];
    
    return filePath;
}








#pragma mark -
#pragma mark Helper methods

-(NSString*)formatDistance:(double)distance {
	return [self _formatDistance:distance isBasic:NO];
}


-(NSString*)formatDistanceBasic:(double)distance {
	return [self _formatDistance:distance isBasic:YES];
}


-(NSString*)_formatDistance:(double)distance isBasic:(BOOL)basic {
	if (self.isMetric == YES) {
		if (distance < 1000 || basic == YES) {
			// Meters
			return [NSString stringWithFormat:@"%d m", (int)distance];
		} else {
			// Kilometers
			return [NSString stringWithFormat:@"%.2f km", distance / 1000.0];
		}
	} else {
		if (distance < (MILE_TO_YARD * 0.25) || basic == YES) {
			// Yards
			return [NSString stringWithFormat:@"%d yards", (int)(distance / YARD_TO_METER)];
		} else {
			// Miles
			double value = (distance / YARD_TO_METER) / (MILE_TO_YARD * 1.0);
			return [NSString stringWithFormat:@"%.2f mile%@", value, value > 1 ? @"s" : @""];
		}
	}
}


-(NSString*)formatSpeed:(double)speed {
	if (speed < 1.0) {
		return @"-";
	}
	else if (self.isMetric == YES) {
		
		// Metric system
		return [NSString stringWithFormat:@"%.1f km/h", (speed / 1000.0) * 3600.0];
		
	} else {
		
		// US system
		return [NSString stringWithFormat:@"%.1f mph", ((speed / 1000.0) / MILE_TO_KM) * 3600.0];
		
	}
}


-(int)getElapsedTimeInMilliseconds {
    
	double delta = 0;
	if (self.startTime == NULL) {
		self.startTime = [NSDate date];
	}
	else {
		delta = fabs([self.startTime timeIntervalSinceNow]);
	}
	
	return self.elapsedTime + (int)(delta * 1000);
	
}


#pragma mark -
#pragma mark CLLocationManager methods

- (void)locationManager:(CLLocationManager*)manager
	didUpdateToLocation:(CLLocation*)newLocation
		   fromLocation:(CLLocation*)oldLocation {
    
	[self processLocationChange:newLocation fromLocation:oldLocation];
	
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
    UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                        message:error.localizedDescription
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                              otherButtonTitles:nil] autorelease];
    [alertView show];
    
    [self.locationManager stopUpdatingLocation];
}

-(void)processLocationChange:(CLLocation*)newLocation fromLocation:oldLocation {
    
	if (newLocation != oldLocation) {
		
		NSLog(@"Moved from %@ to %@", oldLocation, newLocation);
        
        if ([self.locationPoints count] == 0) {
            oldLocation = nil;
        }
        
		CLLocation* lastKnownLocation = NULL;
		if ([self.locationPoints count] > 0) {
			lastKnownLocation = [self.locationPoints objectAtIndex:[self.locationPoints count] - 1];
		}
		else {
			lastKnownLocation = newLocation;
			self.bottomLeft = newLocation.coordinate;
			self.topRight = newLocation.coordinate;
            
            StartPointAnnotation *annotation = [[StartPointAnnotation alloc] init];
            annotation.coordinate = newLocation.coordinate;
            
            [self.statsController addAnnotation:annotation];
            [annotation release];
		}
		
		// Check for new boundaries
		CLLocationCoordinate2D coords = newLocation.coordinate;
		if (coords.latitude < bottomLeft.latitude || coords.longitude < bottomLeft.longitude) {
			self.bottomLeft = coords;
			NSLog(@"Changed bottom left corner");
		}
		if (coords.latitude > topRight.latitude || coords.longitude > topRight.longitude) {
			self.topRight = coords;
			NSLog(@"Changed top right corner");
		}
		
		double speed = fabs(newLocation.speed);
		double deltaDist = fabs([newLocation distanceFromLocation:lastKnownLocation]);
		double newAvgSpeed = (self.totalDistance + deltaDist) / ((double)[self getElapsedTimeInMilliseconds] / 1000.0);
		double accuracy = newLocation.horizontalAccuracy;
		double alt = newLocation.altitude;
		
		NSLog(@"Change in position: %f", deltaDist);
		NSLog(@"Accuracy: %f", accuracy);
        NSLog(@"sensitivity: %f", self.sensitivity);
		NSLog(@"Speed: %f", speed);
		NSLog(@"Avg speed: %f", newAvgSpeed);
		
		if (oldLocation != NULL &&
			(accuracy < 0 ||
             deltaDist < accuracy ||
             deltaDist < self.sensitivity ||
             deltaDist > 10 * self.sensitivity ||
             speed > MAX_SPEED ||
             newAvgSpeed > MAX_SPEED)) {
                
                NSLog(@"Ignoring invalid location change");
                
            }
		else {
			
			NSLog(@"Previous distance = %f", self.totalDistance);
			
			if (self.totalDistance < 0) {
				self.totalDistance = 0;
			}
			
			self.totalDistance += deltaDist;
			self.currentSpeed = speed;
			self.avgSpeed = newAvgSpeed;
			self.altitude = alt;
			
			NSLog(@"Delta distance = %f", deltaDist);
			NSLog(@"New distance = %f", self.totalDistance);
			
            NSLog(@"bgTask = %d", self.bgTask);
            
			// Add new location to path
			[self.locationPoints addObject:newLocation];
			
            //
            if ([self isRunBackground]) {
                
            }
			else {
                NSLog(@"update display");
                // Update stats display
                [self.statsController updateRunDisplay];
                
                // Update map view
                [self updateMap:lastKnownLocation newLocation:newLocation];
            }
		}
		
	}
	
}


- (void)tabBarController:(UITabBarController *)tabBarController
 didSelectViewController:(UIViewController *)viewController {
	
	int index = self.tabBarController.selectedIndex;
	if (index == 0) {
		
		// Main view
		[self.statsController updateRunDisplay];
		
	} else if (index == 1) {
		
		// Settings
		NSLog(@"Initial sensitivity = %f", self.sensitivity);
		float value = (float) ((self.sensitivity - MIN_DIST_CHANGE) * 1.0 / ((MAX_DIST_CHANGE - MIN_DIST_CHANGE) * 1.0));
		[self.settingsController.sensitivitySlider setValue:value];
		
	}
	
}

@end