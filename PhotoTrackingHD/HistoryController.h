//
//  TrackViewController.h
//  PhotoTrackingHD
//
//  Created by 张 乐 on 12-8-20.
//  Copyright (c) 2012年 张 乐. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HistorySearchBar.h"

@class AppDelegate;

@interface HistoryController : UITableViewController {
    NSIndexPath *deleteIndexPath;
}

@property (nonatomic, retain) NSIndexPath *deleteIndexPath;
@property (nonatomic, retain) AppDelegate* delegate;

@property (strong, nonatomic) NSMutableArray *tracks;

@property (retain, nonatomic) IBOutlet HistorySearchBar *searchBar;

@end
