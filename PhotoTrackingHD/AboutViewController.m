//
//  AboutViewController.m
//  PhotoTracking
//
//  Created by 张 乐 on 12-11-19.
//  Copyright (c) 2012年 张 乐. All rights reserved.
//

#import "AboutViewController.h"
#import <MessageUI/MFMailComposeViewController.h>

@interface AboutViewController (MFMailComposeViewControllerDelegate) <MFMailComposeViewControllerDelegate>
@end

@implementation AboutViewController

- (void)dealloc {
    [_email release];
    [super dealloc];
}

-(void)viewDidLoad {
    self.email.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture =
    [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(emailLabelTap)] autorelease];
    [self.email addGestureRecognizer:tapGesture];
}

- (void)viewDidUnload {
    [self setEmail:nil];
    [super viewDidUnload];
}

-(void)emailLabelTap
{
    MFMailComposeViewController *controller = [MFMailComposeViewController new];
    controller.mailComposeDelegate = self;
    [controller setToRecipients:[NSArray arrayWithObjects:@"ieclipse0@gmail.com", nil]];
    
    [self presentModalViewController:controller animated:YES];
}

@end


#pragma mark -
@implementation AboutViewController (MFMailComposeViewControllerDelegate)

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissModalViewControllerAnimated:YES];
}

@end
