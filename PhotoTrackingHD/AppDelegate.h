//
//  AppDelegate.h
//  PhotoTrackingHD
//
//  Created by 张 乐 on 12-8-20.
//  Copyright (c) 2012年 张 乐. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "ObjSqliteDB.h"
#import "config.h"

@class StatsViewController;
@class SettingsViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate, UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (retain) CLLocationManager* locationManager;
@property CLLocationCoordinate2D bottomLeft;
@property CLLocationCoordinate2D topRight;

@property (retain) NSMutableArray* locationPoints;
@property (retain) NSTimer* timer;
@property (retain) NSDate* startTime;
@property double totalDistance;
@property double avgSpeed;
@property double currentSpeed;
@property double altitude;
@property int elapsedTime;

@property BOOL hasZoomedOnMap;
@property BOOL isMetric;
@property double sensitivity;

@property BOOL isBackground;
@property BOOL isRunning;
@property BOOL bgTask;
@property int lastLocationIndex;
@property (retain) ObjSqliteDB* db;

@property (nonatomic, retain) StatsViewController* statsController;
@property (nonatomic, retain) SettingsViewController* settingsController;

@property (nonatomic, retain) UITabBarController* tabBarController;

-(void)reset;
-(void)start;
-(void)stop;
-(void)updateTimer;
-(void)updateMap:(CLLocation*)oldLocation newLocation:(CLLocation*)location;
-(void)processLocationChange:(CLLocation*)newLocation fromLocation:oldLocation;
-(double)generateRamdonChange;
-(double)updateSensitivity;
-(BOOL)updateUnitSystem;
-(BOOL)updateBackground;
-(void)saveSettings;
-(BOOL)loadSettings;

-(NSString*)configFilePath;
-(void)simulateFreewayLocation;
-(BOOL)isRunBackground;
-(void)restoreUnShownLocations;
-(BOOL)isSupportBackgroundTask;
-(BOOL)checkLocationServiceSupported;
-(void)saveLocations;
-(void)initLocationsDB;
-(NSString*)dbFilePath;
-(void)clearMap;

-(NSString*)gpxFilePath:(NSString *)file;
-(NSString*)createGPX:(NSString *)file;

- (void)loadMapType:(MKMapView*)mapView;
- (void)saveMapType:(MKMapView*)mapView;

-(NSArray*)historyList;
-(void)deleteTrack:(NSString*)track_id;
-(void)renameTrack:(NSString*)track_id trackName:(NSString*)trackName;
-(NSMutableSet*)getTrackLocations:(NSString*)trackID;

-(NSString*)_formatDistance:(double)distance isBasic:(BOOL)basic;
-(NSString*)formatDistanceBasic:(double)distance;
-(NSString*)formatDistance:(double)distance;
-(NSString*)formatSpeed:(double)speed;
-(int)getElapsedTimeInMilliseconds;


@end
