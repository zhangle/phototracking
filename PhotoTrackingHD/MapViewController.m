//
//  MapViewController.m
//  PhotoTrackingHD
//
//  Created by 张 乐 on 12-8-20.
//  Copyright (c) 2012年 张 乐. All rights reserved.
//

#import "MapViewController.h"
#import <MessageUI/MFMailComposeViewController.h>
#import <GPX/GPX.h>
#import "TrackPoint.h"
#import "StartPointAnnotation.h"
#import "EndPointAnnotation.h"

@interface MapViewController (UIAlertViewDelegate) <UIAlertViewDelegate>
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
@end

@interface MapViewController (UIActionSheetDelegate) <UIActionSheetDelegate>
- (void)sendMail:(NSString *)filePath;
- (void)mailFile:(NSString *)filePath;
@end

@interface MapViewController (MFMailComposeViewControllerDelegate) <MFMailComposeViewControllerDelegate>
@end

@interface MapViewController (MKMapViewDelegate) <MKMapViewDelegate>
- (void)updateOverlay;
@end

@implementation MapViewController
@synthesize infoView;
@synthesize infoLabel;
@synthesize nameLabel;
@synthesize mapView;
@synthesize track;
@synthesize delegate;
@synthesize trackViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    QBKOverlayMenuViewOffset offset;
    offset.topOffset = 0;
    
    _qbkOverlayMenu = [[QBKOverlayMenuView alloc] initWithDelegate:self position:kQBKOverlayMenuViewPositionTop offset:offset];
    [_qbkOverlayMenu setParentView:[self view]];
    
    [_qbkOverlayMenu addSegmentButton];
    
    [self.delegate loadMapType:self.mapView];
    [_qbkOverlayMenu setSelectedButton:self.mapView.mapType];
    
    self.infoView.backgroundColor = [UIColor colorWithHue:0.0 saturation:0.0 brightness:1.0 alpha:0.4];
    
    if (self.track) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeStyle:NSDateFormatterFullStyle];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        
        NSDate* endTime = [[NSDate alloc] initWithTimeInterval:self.track.elapsedTime sinceDate:self.track.timestamp];
        
        NSString* time = [NSString stringWithFormat:@"%@-%@", [dateFormatter stringFromDate:self.track.timestamp], [dateFormatter stringFromDate:endTime]];
        
        [dateFormatter release];
        [endTime release];
        
        [self.infoLabel setText:[NSString stringWithFormat:@"%@, %d m", time, self.track.distance]];
        [self.nameLabel setText:self.track.name];
        
        [self showLog];
    }
}

- (void)viewDidUnload
{
    [self setMapView:nil];
    [self setInfoLabel:nil];
    [self setNameLabel:nil];
    [self setInfoView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
    [self.track release];
    self.track = nil;
    
    [_qbkOverlayMenu release];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [mapView release];
    mapView = nil;
    [infoLabel release];
    [nameLabel release];
    [infoView release];
    [super dealloc];
}

- (IBAction)action:(id)sender {
    UIActionSheet *actionSheet = [UIActionSheet new];
    actionSheet.delegate = self;
    
    // setup actions
    if ([MFMailComposeViewController canSendMail]) {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Mail this Log", nil)];
    }
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Delete", nil)];
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Rename", nil)];
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    
    // set cancel action position
    actionSheet.cancelButtonIndex = actionSheet.numberOfButtons -1;
    
    [actionSheet showInView:self.view];
}

- (void)showLog
{
    [self updateOverlay];
    
    //
    // Thanks for elegant code!
    // https://gist.github.com/915374
    //
    MKMapRect zoomRect = MKMapRectNull;
    for (TrackPoint *trackPoint in self.track.trackpoints) {
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(trackPoint.latitude.floatValue, trackPoint.longitude.floatValue);
        MKMapPoint annotationPoint = MKMapPointForCoordinate(coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0);
        if (MKMapRectIsNull(zoomRect)) {
            zoomRect = pointRect;
        } else {
            zoomRect = MKMapRectUnion(zoomRect, pointRect);
        }
    }
    [self.mapView setVisibleMapRect:zoomRect animated:NO];
}

#pragma mark - Métodos de QBKOverlayMenuViewDelegate
- (void)overlayMenuView:(QBKOverlayMenuView *)overlayMenuView didActivateAdditionalButtonWithIndex:(NSInteger)index
{
    //NSLog(@"Botón pulsado con índice: %d", index);
    
    self.mapView.mapType = index;
    
    [self.delegate saveMapType:self.mapView];
}

- (void)didPerformUnfoldActionInOverlayMenuView:(QBKOverlayMenuView *)overlaymenuView
{
    //NSLog(@"Menú DESPLEGADO");
}

- (void)didPerformFoldActionInOverlayMenuView:(QBKOverlayMenuView *)overlaymenuView
{
    //NSLog(@"Menú REPLEGADO");
}

@end

@implementation MapViewController (UIAlertViewDelegate)
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != alertView.cancelButtonIndex) {
        if (alertView.title == NSLocalizedString(@"Delete", nil)) {
            [self.delegate deleteTrack:self.track.track_id];
            [self.trackViewController.navigationController popToRootViewControllerAnimated:YES];
        }
        else {
            NSString* newName = [[alertView textFieldAtIndex:0] text];
            
            newName = [newName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            if ([newName length] > 0) {
                [self.delegate renameTrack:self.track.track_id trackName:newName];
                self.track.name = newName;
                [self.nameLabel setText:newName];
            }
        }
    }
}
@end

#pragma mark -
@implementation MapViewController (UIActionSheetDelegate)

-(void)warningCannotSengMail
{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", nil) message:[NSString stringWithFormat:NSLocalizedString(@"Can not send mail", nil), self.track.name] delegate:nil cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:nil, nil];
    
    alert.alertViewStyle = UIAlertViewStyleDefault;
    [alert show];
    [alert release];
}

-(void)sendMail:(NSString *)filePath
{
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil)
    {
        if ([mailClass canSendMail])
        {
            [self mailFile:filePath];
        }
        else
        {
            [self warningCannotSengMail];
        }
    }
    else
    {
        [self warningCannotSengMail];
    }
}

- (void)mailFile:(NSString *)filePath
{
    MFMailComposeViewController *controller = [MFMailComposeViewController new];
    controller.mailComposeDelegate = self;
    
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    [controller addAttachmentData:data mimeType:@"application/gpx+xml" fileName:[filePath lastPathComponent]];
    
    [self presentModalViewController:controller animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != actionSheet.cancelButtonIndex) {
        
        NSString *filePath = [self.delegate gpxFilePath:self.track.name];
        
        if (filePath) {
            if (0 == buttonIndex) {
                [self sendMail:filePath];
            }
            else if (1 == buttonIndex) {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Delete", nil) message:[NSString stringWithFormat:NSLocalizedString(@"Delete the track named", nil), self.track.name] delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"OK",nil), nil];
                
                alert.alertViewStyle = UIAlertViewStyleDefault;
                [alert show];
                [alert release];
            }
            else if (2 == buttonIndex) {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Rename", nil) message:NSLocalizedString(@"Rename the track", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"OK",nil), nil];
                
                alert.alertViewStyle = UIAlertViewStylePlainTextInput;
                
                UITextField *textField = [alert textFieldAtIndex:0];
                textField.placeholder = self.track.name;
                
                [alert show];
                [alert release];
            }
        }
    }
}
@end


#pragma mark -
@implementation MapViewController (MFMailComposeViewControllerDelegate)

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissModalViewControllerAnimated:YES];
}

@end


#pragma mark -
@implementation MapViewController (MKMapViewDelegate)

- (void)updateOverlay
{
    if (!self.track) {
        return;
    }
    
    NSMutableSet* points = [self.delegate getTrackLocations:self.track.track_id];
    
    for (TrackPoint* point in points) {
        [self.track addTrackpointsObject:point];
    }
    
    NSArray *trackPoints = self.track.sotredTrackPoints;
    
    CLLocationCoordinate2D coors[trackPoints.count];
    
    int i = 0;
    for (TrackPoint *trackPoint in trackPoints) {
        coors[i] = trackPoint.coordinate;
        i++;
    }
    
    MKPolyline *line = [MKPolyline polylineWithCoordinates:coors count:trackPoints.count];
    
    // replace overlay
    [self.mapView removeOverlays:self.mapView.overlays];
    [self.mapView addOverlay:line];
    
    StartPointAnnotation *startAnnotation = [[[StartPointAnnotation alloc] init] autorelease];
    startAnnotation.coordinate = coors[0];
    [self.mapView addAnnotation:startAnnotation];
    
    EndPointAnnotation *endAnnotation = [[[EndPointAnnotation alloc] init] autorelease];
    endAnnotation.coordinate = coors[trackPoints.count - 1];
    [self.mapView addAnnotation:endAnnotation];
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay
{
    MKPolylineView *overlayView = [[[MKPolylineView alloc] initWithOverlay:overlay] autorelease];
    overlayView.strokeColor = [UIColor blueColor];
    overlayView.lineWidth = 5.f;
    
    return overlayView;
}


- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation
{
    static NSString *AnnotationViewID = @"annotationViewID";
    
    MKAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
    if (annotationView == nil)
    {
        annotationView = [[[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID] autorelease];
    }
    
    annotationView.annotation = annotation;
    
    if ([annotationView.annotation isKindOfClass:[StartPointAnnotation class]]) {
        annotationView.image = [UIImage imageNamed:@"start.png"];
    }
    else if ([annotationView.annotation isKindOfClass:[EndPointAnnotation class]]) {
        annotationView.image = [UIImage imageNamed:@"end.png"];
    }
    
    return annotationView;
}


@end
