//
//  MapViewController.h
//  PhotoTrackingHD
//
//  Created by 张 乐 on 12-8-20.
//  Copyright (c) 2012年 张 乐. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "AppDelegate.h"
#import "HistoryController.h"
#import "Track.h"
#import "QBKOverlayMenuView.h"

@class AppDelegate;
@class HistoryController;

@interface MapViewController : UIViewController <QBKOverlayMenuViewDelegate> {
    QBKOverlayMenuView *_qbkOverlayMenu;
}

@property (nonatomic, retain) AppDelegate* delegate;
@property (nonatomic, retain) HistoryController* trackViewController;
@property (strong, nonatomic) Track *track;

@property (retain, nonatomic) IBOutlet MKMapView *mapView;
- (IBAction)action:(id)sender;
@property (retain, nonatomic) IBOutlet UIView *infoView;
@property (retain, nonatomic) IBOutlet UILabel *infoLabel;
@property (retain, nonatomic) IBOutlet UILabel *nameLabel;

@end
