//
//  AboutViewController.h
//  PhotoTracking
//
//  Created by 张 乐 on 12-11-19.
//  Copyright (c) 2012年 张 乐. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OHAttributedLabel.h"

@interface AboutViewController : UIViewController {
    OHAttributedLabel *_email;
}

@property (retain, nonatomic) IBOutlet OHAttributedLabel *email;

@end
